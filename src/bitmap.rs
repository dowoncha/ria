use std::rc::Rc;
use std::cell::{Ref, RefCell};

use super::Pixel;

pub struct Bitmap {
    pub width: u32,
    pub height: u32,
    pub row_bytes: u32,
    pub pixels: RefCell<Vec<Pixel>>
}

impl Bitmap {
    pub fn new(width: u32, height: u32) -> Self {
        Self {
            width,
            height,
            row_bytes: (width - 1) * std::mem::size_of::<Pixel>() as u32,
            pixels: RefCell::new(Vec::new())
        }
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn row_bytes(&self) -> u32 {
        self.row_bytes
    }

    // NOTE: decided to use set_pixel method
    // rather than return a mutable ref to the pixel buffer
    // to avoid borrow checker for the moment
    pub fn set_pixel(&self, x: u32, y: u32, pixel: Pixel) {
        let index = (x + y * self.row_bytes) as usize;
        self.pixels.borrow_mut().as_mut_slice()[index] = pixel;
    }

    /*
    pub fn pixels(&self) -> &[Pixel] {
        let borrowed = &self.pixels.borrow();

        borrowed
    }

    // TODO: need to convert into idiomatic Rust
    // Borrowing issues with temp values being dropped
    pub fn mut_pixels(&self) -> *mut [Pixel] {
        let ptr: *mut Vec<Pixel> = self.pixels.as_ptr();

        (*ptr).as_mut_slice()
    }
    */

    // read from file(path)
    // write from file(path)
}
