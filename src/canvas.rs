use super::{Pixel, Color, Bitmap};

pub enum CanvasError {
    IncorrectBitmapDimensions
}


pub struct Canvas<'b> {
    bitmap: &'b Bitmap
}

impl<'b> Canvas<'b> {
    pub fn new(bitmap: &'b Bitmap) -> Result<Canvas, CanvasError> {

        if bitmap.row_bytes() < (bitmap.width() * std::mem::size_of::<Pixel>() as u32)
            || bitmap.pixels.borrow().is_empty() {
            return Err(CanvasError::IncorrectBitmapDimensions);
        }

        Ok(Canvas {
            bitmap
        })
    }

    fn clear(&self, color: Color) {
        // convert color to pixel
        let pixel = Pixel::from(color);
        
        for y in 0..self.bitmap.height() {
            for x in 0..self.bitmap.width() {
                self.bitmap.set_pixel(x, y, pixel);
            }
        }
    }

    pub fn restore(&self) {

    }

    pub fn save(&self) {

    }

    pub fn translate(&self, dx: f32, dy: f32) { }

    pub fn rotate(&self, angle: f32) { }
}
