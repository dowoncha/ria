#![allow(dead_code)]
#![allow(unused_var)]

extern crate x11_dl;

mod bitmap;
mod canvas;
mod window;

pub use self::bitmap::Bitmap;
pub use self::canvas::Canvas;
pub use self::window::Window;

pub type Pixel = u32;
pub struct Color {
    a: f32,
    r: f32,
    g: f32,
    b: f32
}

impl Color {
    pub fn argb(a: f32, r: f32, g: f32, b: f32) -> Self {
        Self {
            a,
            r,
            g,
            b
        }
    }
}

impl From<Color> for Pixel {
    fn from(color: Color) -> Pixel {
        0
    }
}

#[derive(Copy, Clone)]
pub struct Point {
    pub x: f32,
    pub y: f32
}

impl Point {
    fn set(&mut self, x: f32, y: f32) {
        self.x = x;
        self.y = y;
    }
}

