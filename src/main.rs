extern crate ria;

use ria::{Canvas, Window, Color};


struct Rect {

}

impl Rect {
    pub fn left(&self) -> f32 {
        0.0
    }

    pub fn right(&self) -> f32 {
        0.0
    }

    pub fn top(&self) -> f32 {
        0.0
    }

    pub fn bottom(&self) -> f32 {
        0.0
    }
}

trait Shape {
    fn get_rect(&self) -> Rect;

    fn angle(&self) -> f32;

    fn on_draw(&self, canvas: &Canvas);

    fn concat(&self, canvas: &Canvas) {
        let rect = self.get_rect();
        let cx = (rect.left() + rect.right()) / 2.0;
        let cy = (rect.top() + rect.bottom()) / 2.0;

        canvas.translate(cx, cy);
        canvas.rotate(self.angle() * std::f32::consts::PI / 180.0);
        canvas.translate(-cx, -cy);
    }

    fn draw(&self, canvas: &Canvas) {
        canvas.save();
        self.concat(canvas);
        self.on_draw(canvas);
        canvas.restore();
    }
}

struct TestWindow {
    window: Window,
    list: Vec<Box<Shape>>,
    bg_color: Color
}

impl TestWindow {
    pub fn new(width: u32, height: u32) -> TestWindow {
        TestWindow {
            window: Window::new(width, height),
            list: Vec::new(),
            bg_color: Color::argb(1., 1., 1., 1.)
        }
    }

    pub fn run(&self) -> i32 {
        self.window.run()
    }
}

fn main() {
    let window = TestWindow::new(640, 480);
    window.run();
}
