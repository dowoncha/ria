use x11_dl::xlib;

use std::os::raw::{c_uint};
use std::cell::RefCell;

use super::{Point, Canvas};

#[derive(Clone)]
struct Click {
    curr: Point,
    prev: Point,
    orig: Point,
    state: ClickState,
    name: String
}

#[derive(Clone, Copy)]
enum ClickState {
    Down,
    Move,
    Up
}

impl Click {
    pub fn new(loc: Point, name: &str) -> Click {
        Click {
            curr: loc,
            prev: loc,
            orig: loc,
            state: ClickState::Down,
            name: name.to_string()
        }
    }
}

pub struct Window {
    display: *mut xlib::Display,
    window: xlib::Window,
    // canvas
    gc: xlib::GC,
    width: u32,
    height: u32,
    ready_to_quit: bool,
    click: Option<RefCell<Click>>
}

impl Window {
    pub fn new(width: u32, height: u32) -> Window {
        // Load xlib library
        let xlib = xlib::Xlib::open().unwrap();

        let display = unsafe { (xlib.XOpenDisplay)(std::ptr::null()) };

        let screen_no = unsafe { (xlib.XDefaultScreen)(display) };
        let root = unsafe { (xlib.XRootWindow)(display, screen_no) };
        let xwindow = unsafe {
            (xlib.XCreateSimpleWindow)(
                display,
                root,
                0, 0, width as c_uint, height as c_uint, 1,
                (xlib.XBlackPixel)(display, screen_no),
                (xlib.XWhitePixel)(display, screen_no)
            )
        };

        // let _ = xlib::XSelectInput(display, window, select_input_mask);
        let _ = unsafe { (xlib.XMapWindow)(display, xwindow) };

        let gc = unsafe { (xlib.XCreateGC)(display, xwindow, 0, std::ptr::null_mut()) };

        let gwindow = Window {
            display,
            window: xwindow,
            gc,
            width,
            height,
            ready_to_quit: false,
            click: None
        };

        // gwindow.setup_bitmap(width, height);

        gwindow
    }

    fn on_draw(&self, canvas: &mut Canvas) {
    }

    fn handle_event(&self, event: xlib::XEvent) -> bool {
        unsafe {
            match event {
                xlib::XEvent { motion } => {
                    if let Some(ref click) = &self.click {
                        click.borrow_mut().state = ClickState::Move;
                        click.borrow_mut().prev = click.borrow().curr;
                        click.borrow_mut().curr.set(motion.x as f32, motion.y as f32);

                        // this.on_handle_click(self.click);
                        
                        println!("Motion {} {} {}", motion.x, motion.y, motion.state);
                    }
                },
                _ => { }
            }
        }

        false
    }

    // TODO: Convert into a result
    pub fn run(&self) -> i32 {
        if self.display.is_null() { 
            return -1; 
        }

        let xlib = xlib::Xlib::open().unwrap();

        loop {
            let event = std::ptr::null_mut();
            
            unsafe { (xlib.XNextEvent)(self.display, event) };

            // self.handle_event(event as xlib::XEvent);

            if self.ready_to_quit {
                break;
            }
        }

        0
    }
}

impl Drop for Window {
    fn drop(&mut self) {
        // std::mem::drop(self.canvas);
        // free bitmap pixels
        
        if !self.display.is_null() {
            unsafe {
                let xlib = xlib::Xlib::open().unwrap();

                (xlib.XFreeGC)(self.display, self.gc);
                (xlib.XDestroyWindow)(self.display, self.window);
                (xlib.XCloseDisplay)(self.display);
            }
        }
    }
}
