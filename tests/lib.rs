#[cfg(test)]

extern crate ria;

struct TestStats {
    test_counter: i32,
    pass_counter: i32
}

impl TestStats {
    fn did_test(success: bool, msg: &str) { }
}

mod test {
    use std::cell::RefCell;

    use ria::{Pixel,Bitmap, Canvas};

    const PIXEL_MEM_SIZE: u32 = std::mem::size_of::<Pixel>() as u32;

    #[test]
    fn test_bad_input() {
        let mut bitmap = Bitmap::new(10, 10);

        assert!(Canvas::new(&bitmap).is_err());

        bitmap.row_bytes = (bitmap.width + 7) * PIXEL_MEM_SIZE;
        bitmap.pixels.borrow_mut().reserve_exact((bitmap.width * bitmap.height) as usize);

        assert!(Canvas::new(&bitmap).is_err());
    }

    #[test]
    fn test_learn() {
        let width = 10;
        let height = 10;
        let row_bytes = (width + 11) * PIXEL_MEM_SIZE;
        let buffer_size = (row_bytes * height) as usize;
        let pixels = Vec::with_capacity(buffer_size);

        let bitmap = Bitmap {
            width: 10,
            height: 10,
            row_bytes: 21 * PIXEL_MEM_SIZE,
            pixels: RefCell::new(pixels)            
        };

        const wacky_component = 123;

        let canvas = Canvas::new(&bitmap);

        // assert!(
        const wacky_pixel = Pixel::argb(wacky_component,
                                        wacky_component,
                                        wacky_component,
                                        wacky_component);

        canvas.clear(Color::argb(0, 1, 1, 1));
    }
}

